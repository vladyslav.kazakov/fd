require.config({
    paths: {
        jquery: './vendor/jquery/jquery.min',
        lodash: './vendor/lodash/lodash.min',
        bootstrap: './vendor/bootstrap/bootstrap.min'
    },
    shim: {
        bootstrap: {
            deps: ['jquery']
        }
    },
    urlArgs: 'v=' + (new Date()).getTime(),
    waitSeconds: 15
});

require([
    './project.js',
    'jquery',
    'lodash',
    'bootstrap'
],

function(project, $, _) {
    'use strict';

    $(document).attr('title', _.isString(project.title) ? project.title: 'API');

    project.hash = _.defaultTo(project.hash, '');

    if ( !_.isObjectLike(project.versions) )
        return;

    project.versions = createVersionsList(project.versions);
    project.versionsById = _.keyBy(project.versions, function(version) {
       return version.versionId;
    });
    const LAST_VERSION_ID = _.first(project.versions).versionId;

    init();

    window.addEventListener('hashchange',
        function(e) {
            const oldHashOptions = parseHash(e.oldURL);
            const newHashOptions = parseHash(e.newURL);
            if ( oldHashOptions.article !== newHashOptions.article ) {
                setArticleVisibility(oldHashOptions.article, false);
                setArticleVisibility(newHashOptions.article, true);
            }

            if ( project.currentVersionId !== newHashOptions.versionId ) {
                displayVersion(newHashOptions.versionId);
            }
        },
        false
    );

    const hashOptions = parseHash(window.location.hash);
    displayVersion(hashOptions.versionId);
    $('#content').show();
    return;

    function createVersionsList(versions) {
        let list = _.values(_.forEach(versions, function(value, key) {
            value.displayName = key;
        }));

        list = _.sortBy(list, function(value) {
            return value.order;
        }).reverse();

        list.forEach(function(version, index) {
           version.index = index;
        });

        const result = [];
        list.forEach(function(master) {
            list.slice(master.index).forEach(function(slave) {
                result.push({
                    versionId: `${master.alias}-vs-${slave.alias}`,
                    displayName: ( master.index === slave.index ) ? master.displayName : `${master.displayName} compare to ${slave.displayName}`
                });
            });
        });

        return result;
    }

    function parseHash(hash) {
        hash = decodeURIComponent(hash);
        const options = {};
        Object.defineProperty(options, 'hash', {
            enumerable: false,
            value: function() {
                let hash = '';
                if ( _.isString(this.article) )
                    hash = this.article;
                if ( this.versionId !== LAST_VERSION_ID )
                    hash += `;${this.versionId}`;
                return hash;
            }
        });

        const firstHashIndex = hash.lastIndexOf('#');
        if ( firstHashIndex >= 0 ) {
            options.article = hash.substr(firstHashIndex + 1);
        } else {
            options.article = '';
        }

        const firstVersionIndex = options.article.lastIndexOf(';');
        if ( firstVersionIndex >= 0 ) {
            options.versionId = options.article.substr(firstVersionIndex + 1);
            options.article = options.article.substr(0, firstVersionIndex);
        } else {
            options.versionId = LAST_VERSION_ID;
        }

        return options;
    }

    function init() {
        const v = $('#versions');
        project.versions.forEach(function(version) {
            const itemAsHtml = `<li><a href="#">${version.displayName}</a></li>`;
            $(itemAsHtml).appendTo(v).children().on('click', function(e) {
                e.preventDefault();
                const hashOptions = parseHash(window.location.hash);
                hashOptions.versionId = version.versionId;
                window.location.hash = hashOptions.hash();
            });
        });
    }

    function compare(currentVersionId) {
        project.currentVersionId = currentVersionId;
        $('#loader').show();
        $.ajax(`versions/${currentVersionId}.json?hash=${project.hash}`, {
            cache: true,
            dataType: 'json',
            success: function(view) {
                $('#navigation').html(view.navigation);
                $('#articles').html(view.articles);
                setTabSwitch();
                const hashOptions = parseHash(window.location.hash);
                setArticleVisibility(hashOptions.article, true);
                $('#loader').hide();
            }
        });
    }

    function setArticleVisibility(id, isVisible = true) {
        id = `${id.replace(/\//g, '\\\/').replace(/{/g, '\\{').replace(/}/g, '\\}')}`;
        const a = $(`a.article-ref[href="#${id}"]`);
        const li = a.parent();
        if ( _.isEmpty(id) )
            id = '7dd0d18c-fc68-469b-a288-107c175c1c93';
        const article = $(`article#${id}`);
        if ( isVisible ) {
            article.removeClass('hide');
            a.focus();
            li.addClass('active');
        } else {
            article.addClass('hide');
            li.removeClass('active');
        }
    }

    function displayVersion(newVersion) {
        if ( _.isString(newVersion) ) {
            $('#version strong').html(project.versionsById[newVersion].displayName);
            compare(newVersion);
        }
    }

    // Переключатель вкладок
    function setTabSwitch() {
        _.forEach($('ul.nav-tabs'), function(ul) {
            const tabs = [];
            _.forEach(ul.children, function(li) {
                const tabId = li.attributes['tab-id'].value;
                const tab = {
                    li: $(li),
                    div: $(`#${tabId}`)
                };
                tabs.push(tab);
                _.forEach(li.children, function(a) {
                    a.onclick = function(e) {
                        _.forEach(tabs, function(tab) {
                            tab.li.removeClass('active');
                            tab.div.removeClass('active');
                        });
                        tab.li.addClass('active');
                        tab.div.addClass('active');
                    };
                });
            });
        });
    }
});